#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Blinder+;
#pragma link C++ class MyKMatrix+;
#pragma link C++ class RooAbsEffResModel-;
#pragma link C++ class RooAbsGaussModelEfficiency-;
#pragma link C++ class RooBinnedPdf+;
#pragma link C++ class RooCubicSplineFun+;
#pragma link C++ class RooCubicSplineKnot+;
#pragma link C++ class RooDalitzAmplitude-;
#pragma link C++ class RooDalitzTimeCPCBTAG+;
#pragma link C++ class RooEffConvGenContext+;
#pragma link C++ class RooEffResModel+;
#pragma link C++ class RooIpatia2+;
#endif

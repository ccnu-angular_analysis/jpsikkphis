#include "TF1.h"
#include "RooWorkspace.h"
#include "RooSimWSTool.h"
#include "RooUnblindUniform.h"
#include "RooMultiVarGaussian.h"
#include "RooFitResult.h"
#include "RooFit.h"
#include "RooMinuit.h"
#include "RooAddition.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCBShape.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "RooClassFactory.h"
#include "TROOT.h"
#include "TList.h"
#include "RooCategory.h"
#include "RooAbsCategoryLValue.h"
#include "RooGaussModel.h"
#include "RooRealConstant.h"
#include "RooRealVar.h"
#include "RooAddModel.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "TFile.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TRandom3.h"
#include "Roo1DTable.h"
#include "RooTruthModel.h"
#include "RooMCStudy.h"
#include "RooEffProd.h"
#include "RooBinning.h"
#include "RooUniformBinning.h"
#include "RooTFnBinding.h"
#include "RooTDalitz/RooTwoMulGaus.h"
#include "RooTDalitz/RooDalitzAmplitude.h"
#include "RooTDalitz/RooDalitzTimeCPCBTAG.h"
#include "RooTDalitz/RooEffResModel.h"
#include "RooTDalitz/RooBinnedPdf.h"
#include "RooTDalitz/RooCubicSplineFun.h"
#include "RooGaussian.h"
#include "RooBifurGauss.h"
#include "RooChebychev.h"
#include "RooDecay.h"
#include "RooExponential.h"
#include "RooSimultaneous.h"
#include "TSystem.h"
#include <fstream>

using namespace RooFit ;

static double calerr(double* x, int n, double mean, double &err1, double &err2)
{
  double sum1(0),sum2(0);

  double sum(0);
  for(int i=0; i<n; ++i) {
    sum += x[i];
  }
  double ave(sum/(double)n);
  sum=0;
  //  double ave(mean);
  int n1(0), n2(0);
  for(int i=0; i<n; ++i) {
    sum += (x[i]-ave)*(x[i]-ave);
    if(x[i]<mean) {
      sum1 += (x[i]-mean)*(x[i]-mean);
      n1++;
    } else {
      sum2 += (x[i]-mean)*(x[i]-mean);
      n2++;
    }
  }
  err1 = 0;
  err2 = 0;
  if(n1>0)err1 = sqrt(sum1/(double)(n1));
  if(n2>0)err2 = sqrt(sum2/(double)(n2));
  return sqrt(sum/(double)(n));
}

/*const double mhhbins[] = {1.02};
const int nMBins = 2;*/
const double mhhbins[] = {1.008, 1.016, 1.020, 1.024, 1.032};
const int nMBins = 6;
const int nCat = 2;
TString getCutString(const unsigned int i, const char* varname = "X_M"){
    if (i==0) return TString::Format("%s<%.5lf",varname, mhhbins[i]);
    else if (i==nMBins-1) return TString::Format("%s>=%.5lf",varname, mhhbins[i-1]);
    else return TString::Format("%s>=%.5lf&&%s<%.5lf",varname, mhhbins[i-1], varname, mhhbins[i]);
}

TString getCutTrigg(const unsigned int i) {
  if(i==0) return TString("hlt1B>0");
  if(i==1) return TString("hlt1UB>0");
}

RooRealVar* calW2(RooDataSet* dataL, int iset)
{
  double w(0.);
  double w2(0.);
  int num = dataL->numEntries();
  for( int i = 0; i < num; ++i ){
    dataL->get(i); 
    double nsw = dataL->weight();
    w += nsw;
    w2 += pow(nsw,2);  
  }
  double swf2L = w/w2;
  std::cout <<" SW2 " << iset << " " << num << " " << w << " " << swf2L << std::endl;     
  //  double swf  = sqrt(swf2);
  RooRealVar *SWF2L = new RooRealVar(TString::Format("SW_%d",iset).Data(),"Scale ", swf2L);
  return SWF2L;
}

//#define dmConst

//RooAbsReal* GauConstrwCorrelation(RooRealVar* v1, RooRealVar* v2, double c1, double c2, double e1, double e2, double rho, TString name)
//{
//  RooFormulaVar *c  = new RooFormulaVar(name, "(pow((@0-@2)/@4,2)+pow((@1-@3)/@5,2)-2.*@6/@4/@5*(@0-@2)*(@1-@3))/(1.-@6*@6)/2.",RooArgList(*v1, *v2, RooConst(c1), RooConst(c2), RooConst(e1), RooConst(e2), RooConst(rho)));
//  RooFormulaVar *c  = new RooFormulaVar(name, "pow((@0-@1)/@2,2)/2.+pow((@3-@4)/@5,2)/2.",RooArgSet(*v1, RooConst(c1),  RooConst(e1), *v2, RooConst(c2), RooConst(e2) ));
//  return c;
//}
 
 
RooAddition ConstrTerm(RooArgSet &setdlz)
{
  std::cout << "why " << std::endl;
  RooArgSet sumset;
  char varname1[50], varname2[50];
  double mean1,mean2,err1,err2, rho;
  std::ifstream in;

  in.open("inputs/errlist.dat");
  
  int isys(0);
  while(1) {
    in >>  varname1 >> varname2 >> mean1 >> mean2 >> err1 >> err2  >> rho;
    if(!in.good()) break;
    std::cout << "float " << varname1 << " " << mean1 << " " <<  err1 << " " << rho << std::endl;
    std::cout << "float " << varname2 << " " << mean2 << " " <<  err2 << " " << rho << std::endl;
    RooRealVar *var1 = (RooRealVar*)setdlz.find(varname1);
    RooRealVar *var2 = (RooRealVar*)setdlz.find(varname2);
    var1->setConstant(0);
    var1->setRange(mean1-err1*10, mean1+err1*10);
    var2->setConstant(0);
    var2->setRange(mean2-err2*10, mean2+err2*10);
    RooTwoMulGaus *cons = new RooTwoMulGaus( TString(varname1)+"+"+TString(varname2)+"c", "", *var1, *var2, RooConst(mean1), RooConst(err1), 
                 RooConst(mean2), RooConst(err2), RooConst(rho));
    sumset.add(*cons);
    isys++;
  }  
  sumset.Print("V");
  RooAddition sumvar("sumvar","",sumset);
  std::cout << "mw const " << sumvar.getVal() << std::endl;
  return sumvar;  
}
 
RooAbsReal *ConstrTerm1(RooRealVar* var1, RooRealVar* var2)
{
  char varname1[50], varname2[50];
  double mean1,mean2,err1,err2, rho;
  std::ifstream in;

  in.open("inputs/errlist.dat");
  
  while(1) {
    in >>  varname1 >> varname2 >> mean1 >> mean2 >> err1 >> err2  >> rho;
    if(!in.good()) break;
    if(strcmp(varname1,var1->GetName())==0&&strcmp(varname2,var2->GetName())==0) {
      std::cout << "float " << varname1 << " " << mean1 << " " <<  err1 << " " << rho << std::endl;
      std::cout << "float " << varname2 << " " << mean2 << " " <<  err2 << " " << rho << std::endl;
      var1->setConstant(0);
      var1->setRange(mean1-err1*10, mean1+err1*10);
      var2->setConstant(0);
      var2->setRange(mean2-err2*10, mean2+err2*10);
      RooTwoMulGaus *cons = new RooTwoMulGaus( TString(varname1)+"+"+TString(varname2)+"c", "", *var1, *var2, RooConst(mean1), RooConst(err1), 
                 RooConst(mean2), RooConst(err2), RooConst(rho));
      in.close();           
      return cons;
    }           
  }  
  in.close();
  return nullptr;  
}
 

void CreateNLLTwo(bool extendfit=false, bool ftstudy=false)
{
  if(extendfit) std::cout << "Extended fit..." << std::endl;
  if(ftstudy) std::cout << "FT study ..." << std::endl;
  TString phis_blndstr = "BsPhis20152016";
  TString dg_blndstr = "BsDGs20152016";
  double scale_blnd = 0.1;
  
  double  dm0 = 17.768;
  double  edm0= 0.024;
 
  int NoCPU = 1;
  //  double tau0=1.0/0.663;
  double etcut=0.07;
  double ymin(0.98), ymax(1.05);
  
  // Create fit observables
  //  RooRealVar *mass = new RooRealVar("B_ConstJpsi_M","m(J/#psi#pi^{+}#pi^{-})",5300,5440,"MeV");  
  RooRealVar *t = new RooRealVar("t","time [ps]",0.3 ,15);
  RooRealVar *et = new RooRealVar("et","time [ps]",0.0,etcut);
  RooRealVar mpp("X_M","m(#pi^{+}#pi^{-}) [GeV]",ymin,ymax);
  RooRealVar cospp("cosK","cos #theta_{#pi^{+}#pi^{-}}", -1, 1);
  RooRealVar cosH("cosl","cos #theta_{J/#psi}",-1,1);
  RooRealVar chi("phi","#chi", -TMath::Pi(),TMath::Pi());
  RooRealVar nsig_sw("nsig_sw","",0);  
  RooCategory *state1 = new RooCategory("tagOS","tagOS");
  state1->defineType("NTag",0);
  state1->defineType("BS", +1);
  state1->defineType("BSb",-1);
  RooCategory *state2 = new RooCategory("tagSS","tagSS");
  state2->defineType("NTag",0);
  state2->defineType("BS", +1);
  state2->defineType("BSb",-1);
  
  RooRealVar *wtag1 = new RooRealVar("wtagOS", "wtag", 0.,0.5);
  RooRealVar *wtag2 = new RooRealVar("wtagSS", "wtag", 0.,0.5);  
  
  RooRealVar hlt1B("hlt1B","",0);
  RooRealVar hlt1UB("hlt1UB","",0);

  RooArgSet *obs = new RooArgSet(*t, mpp, cospp, cosH, chi);
  obs->add(*state1); obs->add(*wtag1); 
  obs->add(*state2); obs->add(*wtag2);
  obs->add(*et); obs->add(nsig_sw);
  obs->add(hlt1B); obs->add(hlt1UB);  
  
  //fit shared parameters 
  RooRealVar *lambda = new RooRealVar("lambda","",1.0, 0.5, 1.5);
  RooRealVar *dphis = new RooRealVar("phis", "#phi_{s}", 0.07,-TMath::Pi()*2,TMath::Pi()*2,"rad");
  RooUnblindUniform *phis = new RooUnblindUniform("phistrue","",phis_blndstr,scale_blnd, *dphis);
  RooRealVar *Gamma = new RooRealVar("Gamma","", 0.6629, 0.5, 0.8, "ps^{-1}");
  RooFormulaVar *tau = new RooFormulaVar("tau","1/@0",*Gamma);
  RooRealVar *dm = new RooRealVar("DeltaM","deltam", 17.8,15,20,"ps^{-1}");
  RooRealVar *ddg = new RooRealVar("DeltaG","ddg", 0.067, 0.0,0.2,"ps^{-1}"); 
  RooUnblindUniform *dg = new RooUnblindUniform("dgtrue","",dg_blndstr,scale_blnd, *ddg);
  
  //per-event t resolution
  RooRealVar *res_mu    = new RooRealVar("res_mu","mean of resolution fcn",0,"ps");
  RooRealVar *s0 = new RooRealVar("p0","s0", -4.133/1000.);
  RooRealVar *s1 = new RooRealVar("p1","s1", 1.539);
  RooRealVar *s2 = new RooRealVar("p2","s2", 0.);  
  RooRealVar *sc = new RooRealVar("sc","sc",1.0);
  RooFormulaVar *st = new RooFormulaVar("st","abs(@1+@2*@0+@3*@0*@0)*@4",RooArgSet(*et,*s0,*s1,*s2,*sc));
  RooGaussModel *res = new RooGaussModel("res","gau1 for res", *t, *res_mu, *st); 
  //Function for Time ACC 
  RooAbsReal *ACC;
  int nBinsAcceptance = (t->getMax()-t->getMin())/0.01;//1480;
  RooUniformBinning acceptanceBinning(t->getMin(),t->getMax(), nBinsAcceptance,"acceptanceBinning");
  t->setBinning(acceptanceBinning, "acceptanceBinning");

//FixMe by automatic knots from JSON
  std::vector<double> knots;
  knots.push_back(0.3);
  knots.push_back(0.58);
  knots.push_back(0.91);
  knots.push_back(1.35);
  knots.push_back(1.96);
  knots.push_back(3.01);
  knots.push_back(15.00);

  /*
    a0	3.62400e-01 8.87817e-03
    a1	5.95944e-01 8.12072e-03
    a2	7.82916e-01 4.33445e-03
    a3	9.34225e-01 4.16507e-03
    a4	9.86995e-01 3.41793e-03
    a5	9.93854e-01 5.55477e-03
    a6	1.02765e+00 5.54273e-03
    a7	1.0
  */
  RooRealVar * a0 = new RooRealVar("a0", "a0", 1.0);
  RooRealVar * a1 = new RooRealVar("a1", "a1", 0.992);
  RooRealVar * a2 = new RooRealVar("a2", "a2", 1.081);
  RooRealVar * a3 = new RooRealVar("a3", "a3", 1.043);
  RooRealVar * a4 = new RooRealVar("a4", "a4", 1.070);
  RooRealVar * a5 = new RooRealVar("a5", "a5", 1.071);
  RooRealVar * a6 = new RooRealVar("a6", "a6", 1.102);
  RooRealVar * a7 = new RooRealVar("a7", "a7", 0.999);
  RooRealVar * a8 = new RooRealVar("a8", "a7", 0.975);
  RooArgList * coefList = new RooArgList(*a0, *a1, *a2, *a3, *a4, *a5, *a6, *a7, *a8);
  ACC = new RooCubicSplineFun("spline", "spline", *t, knots, *coefList);
  

  
  RooBinnedPdf tacc("tacc","tacc", *t, "acceptanceBinning", *ACC);                     
  tacc.setForceUnitIntegral(true);

 // Define two categories that can be used for splitting
  RooCategory c("c","c") ;
  c.defineType("2016TrgB",0) ;
  c.defineType("2016TrgU",1) ;
  RooWorkspace ws("ws","ws") ; 
//  ws.addClassDeclImportDir("/projects/lhcb/users/zhanglm/workdir/JpsiPhi-Run2/Fit/code-sunliang-v2.1/RooTDalitz");
//  ws.addClassImplImportDir("/projects/lhcb/users/zhanglm/workdir/JpsiPhi-Run2/Fit/code-sunliang-v2.1/src");
//  ws.addClassDeclImportDir("$ROOTSYS/");
//  std::cout << "import " << ws.importClassCode(RooDalitzTimeCPCBTAG::Class(),true) << std::endl;
  ws.import(RooArgSet(tacc,c));
  
  RooSimWSTool sct(ws) ;
  RooSimultaneous* model_sim = sct.build("model_sim","tacc",SplitParam("a0,a1,a2,a3,a4,a5,a6,a7,a8","c")) ;
    
 // Print tree structure of model
//  model_sim->Print("t") ;
  ws.Print();
  RooBinnedPdf *TACC[nCat];
  RooEffResModel* ResAcc[nCat];
  et->setBins( 75, "cache");
  for(int k=0; k<nCat; ++k) {
    c.setIndex(k);
    TACC[k] = (RooBinnedPdf*)ws.pdf("tacc_"+TString(c.getLabel()));
    ResAcc[k] = new RooEffResModel("resacc_"+TString(c.getLabel()),"resacc", *res, *TACC[k] );
    ResAcc[k]->setParameterizeIntegral(RooArgSet(*et));
  }
  
  RooRealVar *acpPro = new RooRealVar("acpPro","B0 Pro ACP", 0.0);
  RooFormulaVar *RPro = new RooFormulaVar("RPro","(@0+1)/(1-@0)", *acpPro);      
  
  TList *cplist = new TList();
  TList *lists[nMBins];
  for (int i=0;i<nMBins;i++) lists[i] = new TList();

//==========phi(1020)==================
  RooRealVar A00_phi("|A0|^2","",0.50934053, 0,0.7);
  RooRealVar Ape_phi("|Aperp|^2","",0.24970058, 0,0.5);

  RooFormulaVar a00_phi("a00_phi","sqrt(@0)",A00_phi);
  RooFormulaVar ape_phi("ape_phi","sqrt(@0)",Ape_phi);
  RooFormulaVar all_phi("all_phi","sqrt(abs(1.-@0-@1))",RooArgList(A00_phi,Ape_phi));  
  RooRealVar phe_phi("delperp-del0","",2.6, -2.*TMath::Pi(),2.*TMath::Pi());
  RooRealVar phl_phi("delpara-del0","",3.1, -2.*TMath::Pi(),2.*TMath::Pi());  
  RooRealVar ph0_phi("del0","",0);
  RooRealVar m0_phi("m0_phi","",1.01941);
  RooRealVar width_phi("width_phi","",0.00443);
  RooArgList* phi_1020 = new RooArgList(a00_phi,ph0_phi,ape_phi,phe_phi,all_phi,phl_phi,m0_phi,width_phi,RooRealConstant::value(1),"phi");
  phi_1020->add(RooRealConstant::value(1)); //type

  //===========f0(980)===================
  RooRealVar m0("m0","m0",0.9499);
  RooRealVar g1("g1","",0.167);
  RooRealVar rg21("rg21","",3.05);
  RooRealVar *A_980s[nMBins];
  RooFormulaVar *a_980s[nMBins];
  RooFormulaVar *ph_980s_a0[nMBins];
  RooRealVar *ph_980s[nMBins];
  RooArgList *f0s[nMBins];
  double phf0[] = {0.84,2.15,0.47,-0.34,-0.59,-0.90};
  double FS0[] = {0.426, 0.059, 0.0101, 0.0103,0.049, 0.193};
  for (int i=0;i<nMBins;i++){
    A_980s[i] = new RooRealVar(TString::Format("FS_%d",i).Data(),
			       "",FS0[i],0.0,100.0);
    a_980s[i] = new RooFormulaVar(TString::Format("a_980_%d",i).Data(),
				  "sqrt(@0/(1.-@0))",*A_980s[i]);              
    ph_980s[i] = new RooRealVar(TString::Format("delS_%d-delperp",i).Data(),
				"",phf0[i],-2.*TMath::Pi(),2.*TMath::Pi());
    ph_980s_a0[i] = new RooFormulaVar(TString::Format("ph_980_a0_%d",i).Data(),
				      "@0+@1",RooArgList(*ph_980s[i],phe_phi));                            
    f0s[i] = new RooArgList(*a_980s[i],*ph_980s_a0[i],m0,g1,rg21,RooRealConstant::value(2),TString::Format("f0_980_%d",i).Data());
  }

  //put list of resonance
  RooRealVar *dphis0 = new RooRealVar("phis0", "#phi_{s}", 0.07,-TMath::Pi()*2,TMath::Pi()*2,"rad");
  RooUnblindUniform *phis0 = new RooUnblindUniform("phis0true","","BsPhiszero20152016",0.2, *dphis0);
  
  RooRealVar *ddphis_e = new RooRealVar("phis_perp-phis0", "phis_perp-phis0", 0, -1, 1,"rad");
  RooUnblindUniform *dphis_e = new RooUnblindUniform("dphis_e_true","","BsPhisperpDel20152016",0.2,*ddphis_e);
  
  RooRealVar *ddphis_l = new RooRealVar("phis_para-phis0", "phis_para-phis0", 0, -1, 1,"rad");
  RooUnblindUniform *dphis_l = new RooUnblindUniform("dphis_l_true","","BsPhisparaDel20152016",0.2,*ddphis_l);
  
  RooRealVar *ddphis_S = new RooRealVar("phis_S-phis0", "phis_S-phis0", 0, -1, 1,"rad");
  RooUnblindUniform *dphis_S = new RooUnblindUniform("dphis_S_true","","BsPhisSDel20152016",0.2,*ddphis_S);
  
  RooFormulaVar *phis_e = new RooFormulaVar("phis_e","@0+@1",RooArgSet(*phis0,*dphis_e));
  RooFormulaVar *phis_l = new RooFormulaVar("phis_l","@0+@1",RooArgSet(*phis0,*dphis_l));
  RooFormulaVar *phis_S = new RooFormulaVar("phis_S","@0+@1",RooArgSet(*phis0,*dphis_S));
  
  //lambda0
  RooRealVar *lambda0 = new RooRealVar("lambda0","",1.0, 0.5, 1.5);
  //lambda
  RooRealVar *rlambda_e = new RooRealVar("rlambda_e","",1.0, 0.5, 1.5);
  RooRealVar *rlambda_l = new RooRealVar("rlambda_l","",1.0, 0.5, 1.5);  
  RooRealVar *rlambda_S = new RooRealVar("rlambda_S","",1.0, 0.5, 1.5);  
  RooFormulaVar *lambda_e = new RooFormulaVar("lambda_e","@0*@1",RooArgSet(*lambda0,rlambda_e));
  RooFormulaVar *lambda_l = new RooFormulaVar("lambda_l","@0*@1",RooArgSet(*lambda0,rlambda_l));  
  RooFormulaVar *lambda_S = new RooFormulaVar("lambda_S","@0*@1",RooArgSet(*lambda0,rlambda_S));  
  
  RooArgList* cp_phi_1020;
  RooArgList* cp_f0_600;  

  if(!extendfit) {  
    cp_phi_1020 = new RooArgList(*lambda,*phis, *lambda,*phis, *lambda,*phis); 
    cp_f0_600 = new RooArgList(*lambda,*phis);  
  } else {
    cp_phi_1020 = new RooArgList(*lambda0, *phis0, *lambda_e, *phis_e, *lambda_l, *phis_l);
    cp_f0_600 = new RooArgList(*lambda_S,*phis_S);  
  }
  
  for (int i=0;i<nMBins;i++){ 
    //      lists[i]->Add(phi_1020s[i]); 
    lists[i]->Add(phi_1020); 
    lists[i]->Add(f0s[i]); }
  cplist->Add(cp_phi_1020);
  cplist->Add(cp_f0_600);
  
//tagging parameters
   RooRealVar p0_OS("p0_OS","",0.389);//,0.3848-5*0.011,0.3848+5*0.011);
   RooRealVar p1_OS("p1_OS","",0.9093);//,1.0-5*0.061,1.0+5*0.061);
   RooRealVar ecOS("eta_bar_OS","",0.384);
   RooRealVar p0_SSK("p0_SSK","",0.4314);//+4.09182e-02);//,0.442-5*0.008,0.442+5*0.008);
   RooRealVar p1_SSK("p1_SSK","",0.8874);//,1.0-5*0.122,1.0+5*0.122);
   RooRealVar ecSSK("eta_bar_SSK","",0.42);
   RooArgList Calibtag(p0_OS,p1_OS,ecOS,p0_SSK,p1_SSK,ecSSK);
   RooRealVar dp0_OS("dp0_OS","",0.0087);
   RooRealVar dp1_OS("dp1_OS","",-0.0063);
   RooRealVar dp0_SSK("dp0_SSK","",-0.0);
   RooRealVar dp1_SSK("dp1_SSK","",0.00);
   RooArgList dCalibtag(dp0_OS,dp1_OS,dp0_SSK,dp1_SSK);
  
  
  //Read Angular acc
  //FixMe  
  double NormW[2][10];
  std::ifstream inw;
  inw.open("inputs/ang3D.txt");
  for(int k=0; k<2; ++k) {
    for(int iw = 0; iw<10; ++iw) inw >> NormW[k][iw];
  }
  inw.close();


   double Csf[] = {0.935, 0.91, 0.86, 0.88, 0.93, 0.93};
   Csf[0] = 0.8563051781469564;
   Csf[1] = 0.8827285118670326;
   Csf[2] = 0.8523857436369079;
   Csf[3] = 0.8881699424366183;
   Csf[4] = 0.944878549926166;
   Csf[5] = 0.9767155926935555;
   std::ifstream inCsf;
   //FixMe for CSf
   inCsf.open("inputs/Csf");
   for(int iw = 0; iw<=5; ++iw) {
     inCsf >> Csf[iw];
     std::cout << "Csf " << iw << " " << Csf[iw] << std::endl;
   }
   inCsf.close();
   
   RooCategory mbin("mbin","mbin") ;
   for(int i=0; i<nMBins; ++i) {
     mbin.defineType(TString::Format("%d",i).Data(),i) ;
   }	

  RooDalitzTimeCPCBTAG *sigts[nCat][nMBins];
  //RooEffResModel *ResAcc[nCat];
  for(int i=0; i<nMBins; ++i) {
    
    //ResAcc[0] = (RooEffResModel*)ws.pdf("resacc_2016TrgB");
    //ResAcc[1] = (RooEffResModel*)ws.pdf("resacc_2016TrgU");    
    for(int k=0; k<nCat; ++k) {
      sigts[k][i] = new RooDalitzTimeCPCBTAG("sig"+TString::Format("{%i;%i}",k,i), "sig", 
					     *t,mpp,cospp, cosH, chi, *state1, *wtag1,
					     *state2, *wtag2, *RPro,
					     Calibtag, dCalibtag,*tau, *dg, *dm,
					     cplist ,lists[i], NormW[k],
					     *ResAcc[k], RooDalitzTimeCPCBTAG::SingleSided,
					     1.5, 5.0, Csf[i], false); 
      // sigts[0][i]->SetParmaters(NormW,Csf[i]);       
    //New for NormW
      // sigts[1][i]->SetParmaters(NormW,Csf[1]);       
    }  
  }

//  std::cout << "Print .... " << std::endl;  

//  sigts[0][0]->Print("t");

  //Generate dataforfit;
  TH1F *hetsig[nCat];
  RooDataHist *etsig[nCat];// = new RooDataHist("etsig","",*et, hetsig);
  RooHistPdf *etsigpdf[nCat];// = new RooHistPdf("etsigpdf","",*et, *etsig);

  RooDataSet *dataL, *subdata[nCat][nMBins];
  RooRealVar *SW2[nCat][nMBins];    
  RooProdPdf *sigall[nCat][nMBins]; 
  
   
  for(int iCat = 0; iCat < nCat; ++iCat ) {
    TString str = TString::Format("%d",iCat);
    TChain *tree = new TChain("tree");
    tree->Add("data2016ForFit.root"); 
    tree->Add("data2015ForFit.root");     

    dataL = new RooDataSet("dataws"+str,"",tree,*obs,"","nsig_sw");
    dataL->Print("V");
    hetsig[iCat] = new TH1F("hetsig"+str,"",15,0.0,etcut);
    dataL->fillHistogram(hetsig[iCat], *et);
    hetsig[iCat]->Scale(1./dataL->sumEntries());
    etsig[iCat] = new RooDataHist("etsig"+str,"",*et, hetsig[iCat]);
    etsigpdf[iCat] = new RooHistPdf("etsigpdf"+str,"",*et, *etsig[iCat]);
    
    for (int i=0;i<nMBins;i++) {
      subdata[iCat][i] = (RooDataSet*)dataL->reduce(getCutString(i)+"&&"+getCutTrigg(iCat));
      SW2[iCat][i] = (RooRealVar*)calW2(subdata[iCat][i],iCat*nMBins+i);
      sigall[iCat][i] = new RooProdPdf(TString::Format("sigall_%d%d",i,iCat).Data(), "mass*t", RooArgSet(*etsigpdf[iCat]), Conditional(*sigts[iCat][i],RooArgSet(*t,mpp, cospp, cosH, chi,*wtag1, *state1, *wtag2, *state2)));       
    }
    dataL->Delete();
//    dataL = new RooDataSet(TString(dataws->GetName())+TString("new"),dataws->GetTitle(),dataws,*dataws->get(),0,"nsig_sw");
    tree->Delete();
  }
   

   
//  double phf0[] = {0.84,2.15,0.47,-0.34,-0.59,-0.90};
//  double FS0[] = {0.426, 0.059, 0.0101, 0.0103,0.049, 0.193};
   //set parameters   
//   ws.var("a3_2016TrgU")->setVal(0.9); 
   m0_phi.setVal(1.019461);
   width_phi.setVal(4.266e-03);   
   
   double nCPU = 1;
   RooAbsReal *nll;
   //constrain term
   RooAbsReal *nllbin[nCat][nMBins];
//   RooArgSet pdfs;
   for(int k=0; k<nCat; k++) {
     for (int i=0;i<nMBins;i++){
       //set parameters
//       pdfs.add(*sigall[k][i]);
       nllbin[k][i] = sigall[k][i]->createNLL(*subdata[k][i],Optimize(0),Extended(0));
       printf("nll {%d,%d} %10.2f \n", k, i,  nllbin[k][i]->getVal());
     }
   }
   RooArgSet setdlz;//[nCat][nMBins];
//    = sigts[0][2]->getParameters(*dataL);

   RooArgSet nllset;//(*nllbin[0],*SW2[0][0]);
   RooAbsReal *eachnll[nCat][nMBins];
   
   for(int k=0; k<nCat; k++) {
     for(int i=0; i<nMBins;i++) {
       setdlz.add(*(sigts[k][i]->getParameters(*dataL)));
     }
   }

   setdlz.readFromFile("inputs.func");
//constraint term
    RooAbsReal* Constr[10];

   
//   setdlz.Print("V");   
   setdlz.Print("V");
   for(int k=0; k<nCat; k++) {
     for(int i=0; i<nMBins;i++) {   
       eachnll[k][i] = new RooFormulaVar("nllset"+TString::Format("{%d;%d}",k,i),"@0*@1",RooArgSet(*nllbin[k][i], *SW2[k][i]));
       nllset.add( *eachnll[k][i] );
     }
   }
   if(ftstudy) {
       Constr[0] = ConstrTerm1( &p0_OS, &p1_OS );
       if( Constr[0] ) nllset.add( *Constr[0] );
       Constr[1] = ConstrTerm1( &p0_SSK, &p1_SSK );
       if( Constr[1] ) nllset.add( *Constr[1] );
       Constr[2] = ConstrTerm1( &dp0_OS, &dp1_OS );
       if( Constr[2] ) nllset.add( *Constr[2] );
       Constr[3] = ConstrTerm1( &dp0_SSK, &dp1_SSK );
       if( Constr[3] ) nllset.add( *Constr[3] );

   }
   nll = new RooAddition("nll","nll", nllset);
//   if(ftstudy) {
//     nll = new RooFormulaVar("nll","@0+0.5*pow((@1-0.38320)/0.0024,2)",RooArgSet(*nll,p0OS));
//     nll = new RooFormulaVar("nll","@0+@1",RooArgSet(*nll,*cons));
//   }
   
/*   RooWorkspace wsall("wsall","wsall");
   wsall.import(pdfs);
   wsall.Print();
   
   RooArgSet setall(wsall.allVars());
   setall.Print("V");
  */
   
   RooMinuit m(*nll);
   m.setErrorLevel(0.5);
   
   if(1) {
     m.setStrategy(1);
//     m.setVerbose(kTRUE);
     m.setPrintLevel(2);
     printf("***NLL = %15.2f\n", nll->getVal());  
     m.migrad();
     m.hesse();
     printf("***NLL = %15.2f\n", nll->getVal());
     //  m.hesse();  
   }   
   setdlz.Print("V");
   if(!extendfit&&!ftstudy) setdlz.writeToFile("func/fit16.func");
   if(!extendfit&&ftstudy) setdlz.writeToFile("func/fit16-ft.func");   
   if(extendfit&&!ftstudy) setdlz.writeToFile("func/fit16-ext.func");   
   if(extendfit&&ftstudy) setdlz.writeToFile("func/fit16-ext-ft.func");      
   return;
}


int main(int argc, char **argv)
{
 
  TDatime BeginTime;
  std::cout << "Time(begin)  " << BeginTime.GetYear() << "." << BeginTime.GetMonth() << "." << BeginTime.GetDay() << "    " << BeginTime.GetHour() << ":" << BeginTime.GetMinute() << ":" << BeginTime.GetSecond() << std::endl; 
  
  int hesse = 1; 
  bool isel = 0;
  
  //std::cout << "argc " << argc << std::endl;

  
  bool ext(false), ft(false);

  if( argc >=2 )  ext = atoi( argv[1] );
  if( argc >=3 ) ft = atoi( argv[2] );
  
  CreateNLLTwo(ext, ft);
//  RooArgSet nll_list;
//  nll_list.add(CreateNLLOne("data2016ForFit_0.20","2016"));
//  nll_list.add(CreateNLLOne("data2016ForFit_0.20","2015"));

   return 1;
}

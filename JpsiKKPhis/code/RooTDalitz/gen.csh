#!/bin/tcsh -f
foreach i (`find RooTwoMulGaus.h`)
 set name = `echo $i |sed "s/.h//g"`
 set filename = "${name}LinkDef.h"
 cp LinkDef $filename
 echo "#pragma link C++ class $name+;" >> $filename
 echo " " >> $filename
 echo "#endif " >> $filename
 echo " " >> $filename
 
end

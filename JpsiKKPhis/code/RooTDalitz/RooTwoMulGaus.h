/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 *****************************************************************************/
#ifndef ROO_TWOMULGAUS
#define ROO_TWOMULGAUS

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
//#include "RooTrace.h"
#include "RooAbsReal.h"

class RooTwoMulGaus : public RooAbsReal {
public:
  //  RooTwoMulGaus() {   TRACE_CREATE } ;
  RooTwoMulGaus() {  };
  RooTwoMulGaus(const char *name, const char *title,
	      RooAbsReal& _x, RooAbsReal& _y, RooAbsReal& _mean1, RooAbsReal& _sigma1 , 
	      RooAbsReal& _mean2, RooAbsReal& _sigma2, RooAbsReal& _rho);
  RooTwoMulGaus(const RooTwoMulGaus& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new RooTwoMulGaus(*this,newname); }
  //  inline virtual ~RooTwoMulGaus() { TRACE_DESTROY };
  inline virtual ~RooTwoMulGaus() {  };

  //Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  //Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

  //Int_t getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t staticInitOK=kTRUE) const;
  //void generateEvent(Int_t code);

  Double_t getLogVal() const ;

protected:

  RooRealProxy x ;
  RooRealProxy y;
  RooRealProxy mean1 ;
  RooRealProxy sigma1 ;
  RooRealProxy mean2 ;
  RooRealProxy sigma2 ;
  RooRealProxy rho;
  
  Double_t evaluate() const ;

private:

  ClassDef(RooTwoMulGaus,1) // Gaussian PDF
};

#endif

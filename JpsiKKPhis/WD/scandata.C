{
//  gROOT->ProcessLineSync(".x RooIpatia2.cxx+") ;

  gROOT->ProcessLine(".x ~/lhcbStyle.C");

  // Create observables

  TChain *h1 = new TChain("tree");
  h1->Add("mcdg0.root");
  h1->SetScanField(0);
//  h1->Scan("b_nopv_M0:B_ConstJpsiNoPV_M[0]","abs(b_nopv_M0-B_ConstJpsiNoPV_M[0])>1e-10");
//  h1->Scan("B_ConstJpsiNoPV_M:X_M:t:et:cosK:cosl:(phi+TMath::Pi())-2*TMath::Pi()*(phi>0):tagOS:wtagOS:tagSS:wtagSS:nsig_sw");
  h1->Scan("B_ConstJpsiNoPV_M:X_M:t:et:cosK:cosl:phi:tagOS:0.0:0:0.5:1:t0");
  exit(0);
//h1->Add("BsJpsiPhi_MC_2016_Sim09b_UpDown_mdst_mvacut.root");
}

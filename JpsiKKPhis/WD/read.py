#/bin/python -f

def GetParameters( arg ):
  my_dict = {}

  with open(arg, 'r') as f:
    for line in f:
      items = line.split()
      key, values = items[0], items[1:]
      my_dict[key] = values
      print float(values[0])
  return my_dict

import json
json.encoder.FLOAT_REPR = lambda f: ("%.5f" % f)
ResultSet={}

poldep=1
FileOut="test.json"
#Here I read the log file
Results=GetParameters("result/fitpol")

parList=[]

#parList.append({"Name":"phis","Value":0.1245,"Error":0})
for key in Results:
  parList.append({"Name":key,"Value":float(Results[key][0]),"Error":float(Results[key][1])})

ResultSet.update({"Parameter":parList})


if not poldep:
  ResultSet.update({"ResultSetLabel":"Tsinghua-Run2",
                    "Description:":"Contains the current polarity independent baseline result from Tsinghua"})
else:
  ResultSet.update({"ResultSetLabel":"Tsinghua-Run2",
                    "Description:":"Contains the current polarity dependent baseline result from Tsinghua"})

with open(FileOut, 'w') as f:
  json.dump(ResultSet, f, indent=4) 


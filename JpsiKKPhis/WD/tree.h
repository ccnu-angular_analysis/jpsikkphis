//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jun 30 08:58:17 2017 by ROOT version 6.02/03
// from TTree tree/2016 data
// found on file: ./mcdg0.root
//////////////////////////////////////////////////////////

#ifndef tree_h
#define tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class tree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        B_ConstJpsiNoPV_M;
   Double_t        X_M;
   Double_t        t;
   Double_t        et;
   Double_t        cosK;
   Double_t        cosl;
   Double_t        phi;
   Int_t           tagOS;
   Double_t        t0;
   Double_t        wpdf;

   // List of branches
   TBranch        *b_B_ConstJpsiNoPV_M;   //!
   TBranch        *b_X_M;   //!
   TBranch        *b_t;   //!
   TBranch        *b_et;   //!
   TBranch        *b_cosK;   //!
   TBranch        *b_cosl;   //!
   TBranch        *b_phi;   //!
   TBranch        *b_tagOS;   //!
   TBranch        *b_t0;   //!
   TBranch        *b_wpdf;   //!

   tree(TTree *tree=0);
   virtual ~tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef tree_cxx
tree::tree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("./mcdg0.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("./mcdg0.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

tree::~tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("B_ConstJpsiNoPV_M", &B_ConstJpsiNoPV_M, &b_B_ConstJpsiNoPV_M);
   fChain->SetBranchAddress("X_M", &X_M, &b_X_M);
   fChain->SetBranchAddress("t", &t, &b_t);
   fChain->SetBranchAddress("et", &et, &b_et);
   fChain->SetBranchAddress("cosK", &cosK, &b_cosK);
   fChain->SetBranchAddress("cosl", &cosl, &b_cosl);
   fChain->SetBranchAddress("phi", &phi, &b_phi);
   fChain->SetBranchAddress("tagOS", &tagOS, &b_tagOS);
   fChain->SetBranchAddress("t0", &t0, &b_t0);
   fChain->SetBranchAddress("wpdf", &wpdf, &b_wpdf);
   Notify();
}

Bool_t tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t tree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef tree_cxx

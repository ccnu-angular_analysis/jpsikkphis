using namespace RooFit ;
std::vector<double> vcoshh, vcosH, vchi;
double Term(double cosh, double cosH, double phi, int it);
static double calerr(double* x, int n, double mean, double &err1, double &err2)
{
  double sum1(0),sum2(0);  
  
  double sum(0);
  for(int i=0; i<n; ++i) {
    sum += x[i];
  }
  double ave(sum/(double)n);
  sum=0;
  //  double ave(mean);
  int n1(0), n2(0);
  for(int i=0; i<n; ++i) {
    sum += (x[i]-ave)*(x[i]-ave);
    if(x[i]<mean) {
      sum1 += (x[i]-mean)*(x[i]-mean);
      n1++;
    } else {
      sum2 += (x[i]-mean)*(x[i]-mean);
      n2++;
    }
  }
  err1 = 0;
  err2 = 0;
  if(n1>0)err1 = sqrt(sum1/(double)(n1));
  if(n2>0)err2 = sqrt(sum2/(double)(n2));
  return sqrt(sum/(double)(n));
}


void GetCo(TH3F* eff3D, double *rco)
{
  TH1F hchi("hchi","",50,-TMath::Pi(),TMath::Pi());
  TH1F hcos("hcos","",50,-1,1);
  double coeff[10];
  double x,y,z;
  for(int ic=0; ic<10; ++ic) coeff[ic]=0;
  for(int i=1; i<=50; ++i) {
    x=hchi.GetBinCenter(i);
    for(int j=1; j<=50; ++j) {			
      y = hcos.GetBinCenter(j);
      for(int k=1; k<=50; ++k) {
	z = hcos.GetBinCenter(k);
	for(int ic=0; ic<10; ++ic) {
	  coeff[ic]+= Term(y, z, x, ic+1) * eff3D->GetBinContent(i,j,k);
	}
      }
    }
  }
  for(int ic=1; ic<10; ++ic)     rco[ic-1]=coeff[ic]/coeff[0];
}

double Term(double cosh, double cosH, double phi, int it)
{
  double cosh2 = cosh*cosh;
  double cosH2 = cosH*cosH;
  if(it==1) return 2.*cosh2*(1.-cosH2);
  if(it==2) return (1.-cosh2)*(1.-(1-cosH2)*pow(cos(phi),2));
  if(it==3) return (1.-cosh2)*(1.-(1-cosH2)*pow(sin(phi),2));
  if(it==4) return (1.-cosh2)*(1.-cosH2)*sin(2.*phi);
  if(it==7) return 2.*(1.-cosH2)/3.;  
  if(it==10) return 4./sqrt(3.)*cosh*(1.-cosH2);  
  double sinh = sqrt(1.-cosh2);
  double sinH = sqrt(1.-cosH2);  
  if(it==5) return 2.*sqrt(2.)*sinh*cosh*sinH*cosH*cos(phi);
  if(it==6) return -2.*sqrt(2.)*sinh*cosh*sinH*cosH*sin(phi);
  if(it==8) return sinh*2.*sqrt(6.)*sinH*cosH*cos(phi)/3.;
  if(it==9) return -sinh*2.*sqrt(6.)*sinH*cosH*sin(phi)/3.;

}


void accYH()
{

  double mBs(5366.3);  
//   std::ifstream in;
//   in.open("func/card_ft.dat");
//   in >> nsig >> nbkg >> npsik >> netp >> rf2_psik >> rf2_m;
//   in.close();
//   std::cout << nsig << " " << nbkg << " " << rf2_psik << std::endl;

  double ymin(0.99), ymax(1.05);

  // Create observables
  

  TFile *feff = new TFile("mcdg0.root");
  TTree *tree= (TTree*)feff->Get("tree");
  TH3F *eff3D = new TH3F("eff3D","",50,-TMath::Pi(),TMath::Pi(),50,-1,1,50,-1,1);
//  TH3F* data3D = (TH3F*)dataws->createHistogram("data3D",chi,Binning(50,-TMath::Pi(),TMath::Pi()),YVar(cospp,Binning(50,-1,1)),ZVar(cosH,Binning(50,-1,1)));
  eff3D->Sumw2(); 
  tree->Project("eff3D","cosl:cosK:phi","1./wpdf*(t>0.3)");
  double rco0[9];
  GetCo(eff3D, rco0);
  for(int ic=1; ic<10; ++ic)     printf("%10.6f\n", rco0[ic-1]);
 
  std::cout << eff3D->GetBinContent(10,25,25) << " " << eff3D->GetBinError(10,25,25) << std::endl;
  double rco[9][500]; 
  double xrco[9];
  char name[100];
  
  TRandom3 *rnd = new TRandom3();  
  static int N=500;
  for(int iset=0;iset<N; ++iset) {
    //    std::cout << "test i " << i << std::endl;
    TH3F *eff3Dnew = (TH3F*)eff3D->Clone("eff3Dnew");
    for(int i=1; i<=50; ++i) {//chi
      for(int j=1; j<=50; ++j) {   //cosh               
        for(int k=1; k<=50; ++k) {//cosH
          eff3Dnew->SetBinContent(i,j,k,eff3D->GetBinContent(i,j,k)+ rnd->Gaus()* eff3D->GetBinError(i,j,k));
          if(eff3Dnew->GetBinContent(i,j,k)<0) eff3Dnew->SetBinContent(i,j,k,0);	  
	}
      }
    }
    
    GetCo(eff3Dnew, xrco);
    for(int ic=0; ic<9;++ic) rco[ic][iset] = xrco[ic];
    sprintf(name,"acc/acc%03d.root",iset);
//    TFile *file = new TFile(name,"recreate");
//    eff3Dnew->Write();
//    file->Close();            
    //    for(int ic=1; ic<10; ++ic)     printf("cof %i %7.4f\n", ic+1, rco[i][ic-1]);    
  }
  double cen, err1, err2, err;
  for(int i=0; i<9; ++i) {
    cen = rco0[i];
    err = calerr(rco[i], N, cen, err1, err2);
    printf("FFF %i %7.4f %7.4f %7.4f %7.4f \n", i, cen, err, err1, err2);  
  }
  exit(0);
}


